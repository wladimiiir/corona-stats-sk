export interface GeneralStats {
  totalInfected: number;
  totalInfectedIncrease: number;
  totalRecovered: number;
  totalRecoveredIncrease: number;
  totalDeaths: number;
  totalDeathsIncrease: number;
}

export interface Stats {
  [date: string]: DailyStats[];
}

export interface DailyStats {
  city: string;
  count: number;
}
