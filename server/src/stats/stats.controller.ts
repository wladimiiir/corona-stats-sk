import {Controller, Get} from '@nestjs/common';
import {StatsService} from 'stats/stats.service';
import {GeneralStats, Stats} from '@common/types';

@Controller('stats')
export class StatsController {
    constructor(private readonly service: StatsService) {
    }

    @Get('/latest')
    async getLatest(): Promise<any> {
        const latest = await this.service.getLatest();
        return {
            totalCount: latest.reduce((count, data) => count + data.count, 0),
            data: latest
        };
    }

    @Get('/general')
    async getGeneral(): Promise<GeneralStats> {
        return this.service.getGeneral();
    }

    @Get('/daily')
    async getDaily(): Promise<Stats> {
        return this.service.getDaily();
    }

    @Get('/city')
    getCities(): string[] {
        return this.service.getCities();
    }
}
