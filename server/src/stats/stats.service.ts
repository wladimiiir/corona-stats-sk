import _ from 'lodash';
import moment from 'moment';
import {Cron} from '@nestjs/schedule';
import {HttpService, Injectable, OnApplicationBootstrap} from '@nestjs/common';
import {DailyStats, GeneralStats, Stats} from '@common/types';

const START_DATE = new Date(2020, 2, 7);
const BI_REQUEST = {
    version: '1.0.0',
    queries: [
        {
            Query: {
                Commands: [
                    {
                        SemanticQueryDataShapeCommand: {
                            Query: {
                                Version: 2,
                                From: [
                                    {
                                        Name: 'w',
                                        Entity: 'Web - Pozitivne nalezy'
                                    }
                                ],
                                Select: [
                                    {
                                        Column: {
                                            Expression: {
                                                SourceRef: {
                                                    Source: 'w'
                                                }
                                            },
                                            Property: 'patient_addressOfStay_City'
                                        },
                                        Name: 'Web - Pozitivne nalezy.patient_addressOfStay_City'
                                    },
                                    {
                                        Aggregation: {
                                            Expression: {
                                                Column: {
                                                    Expression: {
                                                        SourceRef: {
                                                            Source: 'w'
                                                        }
                                                    },
                                                    Property: 'patient_ordinal_number'
                                                }
                                            },
                                            Function: 2
                                        },
                                        Name: 'Web - Pozitivne nalezy.patient_ordinal_number'
                                    }
                                ],
                                Where: [
                                    {
                                        Condition: {
                                            And: {
                                                Left: {
                                                    Comparison: {
                                                        ComparisonKind: 2,
                                                        Left: {
                                                            Column: {
                                                                Expression: {
                                                                    SourceRef: {
                                                                        Source: 'w'
                                                                    }
                                                                },
                                                                Property: 'covid19_confirmed_positive_at'
                                                            }
                                                        },
                                                        Right: {
                                                            Literal: {
                                                                Value: 'datetime\'$START_DATE$\''
                                                            }
                                                        }
                                                    }
                                                },
                                                Right: {
                                                    Comparison: {
                                                        ComparisonKind: 3,
                                                        Left: {
                                                            Column: {
                                                                Expression: {
                                                                    SourceRef: {
                                                                        Source: 'w'
                                                                    }
                                                                },
                                                                Property: 'covid19_confirmed_positive_at'
                                                            }
                                                        },
                                                        Right: {
                                                            Literal: {
                                                                Value: 'datetime\'$END_DATE$\''
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }, {
                                        Condition: {
                                            Not: {
                                                Expression: {
                                                    Comparison: {
                                                        ComparisonKind: 0,
                                                        Left: {
                                                            Column: {
                                                                Expression: {
                                                                    SourceRef: {
                                                                        Source: 'w'
                                                                    }
                                                                },
                                                                Property: 'covid19_confirmed_positive_at'
                                                            }
                                                        },
                                                        Right: {
                                                            Literal: {
                                                                Value: 'null'
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                ]
                            },
                            Binding: {
                                Primary: {
                                    Groupings: [
                                        {
                                            Projections: [0, 1]
                                        }
                                    ]
                                },
                                DataReduction: {
                                    DataVolume: 3,
                                    Primary: {
                                        Window: {
                                            Count: 500
                                        }
                                    }
                                },
                                Version: 1
                            }
                        }
                    }
                ]
            },
            QueryId: '',
            ApplicationContext: {
                DatasetId: '81fedac7-1257-4027-91d2-a3fc86830a49',
                Sources: [
                    {
                        ReportId: '62dabfaa-68de-4f9f-a3b2-e69a13bf6ac4'
                    }
                ]
            }
        }
    ],
    cancelQueries: [],
    modelId: 3429401
};

@Injectable()
export class StatsService implements OnApplicationBootstrap {
    private generalStats: GeneralStats = {
        totalInfected: 0,
        totalInfectedIncrease: 0,
        totalDeaths: 0,
        totalDeathsIncrease: 0,
        totalRecovered: 0,
        totalRecoveredIncrease: 0
    };
    private dailyStats: Stats = {};
    private lastNonEmptyDailyData: DailyStats[];

    constructor(private httpService: HttpService) {
    }

    onApplicationBootstrap(): any {
        this.refreshStats();
    }

    @Cron('0 0 * * * *')
    async refreshStats() {
        await this.refreshGeneralStats();
        await this.refreshDailyStats();
    }

    private async refreshGeneralStats() {
        const data = (await this.httpService.get('https://covid-19.nczisk.sk/webapi/v1/kpi').toPromise()).data;
        const recoveredData = data.tiles.k7.data.d;
        const deathsData = data.tiles.k8.data.d;
        this.generalStats.totalRecovered = recoveredData[recoveredData.length - 1].v;
        this.generalStats.totalRecoveredIncrease = recoveredData[recoveredData.length - 1].v - recoveredData[recoveredData.length - 2].v;
        this.generalStats.totalDeaths = deathsData[deathsData.length - 1].v;
        this.generalStats.totalDeathsIncrease = deathsData[deathsData.length - 1].v - deathsData[deathsData.length - 2].v;
    }

    private async refreshDailyStats() {
        let start = moment(START_DATE).utc().startOf('day');

        let totalInfected = 0;
        let totalInfectedIncrease = 0;
        const dailyStats = {};

        while (!start.isAfter(moment().startOf('day'))) {
            const end = moment(start).endOf('day');

            const dayData = await this.loadDailyData(start, end);
            const key = start.toISOString();

            if (!dailyStats[key]) {
                dailyStats[key] = [];
            }

            const dailyData = dayData.results[0].result.data.dsr.DS[0].PH[0].DM0;
            let lastCount = 1;
            let dailyIncrease = 0;
            for (const data of dailyData) {
                lastCount = Number(data.C[1] || lastCount);
                dailyIncrease += lastCount;
                dailyStats[key].push({
                    city: String(data.C[0]),
                    count: lastCount
                });
            }
            if (dailyData.length > 0) {
                this.lastNonEmptyDailyData = dailyStats[key];
            }
            totalInfected += dailyIncrease;
            totalInfectedIncrease = dailyIncrease;

            start = start.add(1, 'day');
        }

        this.dailyStats = dailyStats;
        this.generalStats.totalInfected = totalInfected;
        this.generalStats.totalInfectedIncrease = totalInfectedIncrease;
    }

    private async loadDailyData(start: moment.Moment, end: moment.Moment) {
        return (await this.httpService.post(
            'https://wabi-west-europe-api.analysis.windows.net/public/reports/querydata?synchronous=true',
            JSON.parse(
                JSON.stringify(BI_REQUEST)
                    .replace('$START_DATE$', start.toISOString())
                    .replace('$END_DATE$', end.toISOString())
            ),
            {
                headers: {
                    'Content-Type': 'application/json',
                    'X-PowerBI-ResourceKey': '49ef274e-400a-425c-978b-a25e58a74d04'
                }
            }
        ).toPromise()).data;
    }

    getCities() {
        return _.uniq(
            Object.values(this.dailyStats)
                .reduce((cities, data) => cities.concat(...data.map(item => item.city)), [] as string[])
        ).sort((a: string, b: string) => a.localeCompare(b));
    }

    getDaily() {
        return this.dailyStats;
    }

    getGeneral() {
        return this.generalStats;
    }

    async getLatest() {
        await this.refreshStats();
        return this.lastNonEmptyDailyData;
    }
}
