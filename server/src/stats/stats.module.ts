import {HttpModule, Module} from '@nestjs/common';
import {StatsService} from './stats.service';
import {ScheduleModule} from '@nestjs/schedule';
import { StatsController } from './stats.controller';

@Module({
    imports: [
        ScheduleModule.forRoot(),
        HttpModule
    ],
    providers: [StatsService],
    controllers: [StatsController]
})
export class StatsModule {
}
