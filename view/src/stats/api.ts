import {callApi} from "app/api";

export const getCities = () => callApi("/stats/city");

export const getGeneralStats = () => callApi("/stats/general");

export const getDailyStats = () => callApi("/stats/daily");
