import React, {useMemo} from "react";
import {Table} from "antd";

import {CityStats} from "stats/types";

type Props = {
    loading?: boolean;
    stats: CityStats;
    selected: string[];
    selectionChanged(selected: string[]);
}

const CityTable = ({loading, stats, selected, selectionChanged}: Props) => {
    const tableData = useMemo(() => {
        const data = [] as any[];
        for (const city in stats) {
            data.push({
                key: city,
                city,
                ...stats[city]
            });
        }
        data.sort((a, b) => {
            const result = b.count - a.count;
            return result || a.city.localeCompare(b.city);
        });
        return data;
    }, [stats]);
    const columns = useMemo(() => (
        [
            {
                title: "City",
                dataIndex: "city",
                render: (text, {increase}) => increase ? <b>{text}</b> : text,
                sorter: {
                    compare: (a, b) => a.city.localeCompare(b.city)
                }
            },
            {
                title: "Total",
                dataIndex: "count",
                align: "right",
                render: (text, {increase}) => increase ? <span>{text} <span className="smallText">(+{increase})</span></span> : text
            }
        ] as any[]), []
    );

    const rowSelection = {
        columnWidth: 50,
        onChange: selectionChanged,
        selectedRowKeys: selected,
        hideDefaultSelections: true
    };

    return (
        <div className="cityTable">
            <Table
                loading={loading}
                dataSource={tableData}
                columns={columns}
                rowSelection={rowSelection}
                pagination={false}
                bordered
            />
        </div>
    );
};

export default CityTable;
