import React, {useEffect, useMemo, useState} from "react";
import {Card, Spin, Statistic} from "antd";
import {ArrowDownOutlined, ArrowUpOutlined} from "@ant-design/icons";
import {isYesterday, startOfDay, startOfYesterday} from "date-fns";
import storage from "localforage";

import {getDailyStats, getGeneralStats} from "stats/api";
import {GeneralStats, Stats} from "@common/types";
import {CityStats} from "stats/types";
import {LineChart} from "ui/components";
import {LineChartData} from "ui/components/LineChart/LineChart";
import {CityTable} from "stats/components";
import {useBooleanState} from "common/hooks";

import "./daily.scss";

const VISIBLE_CITIES_KEY = "visible-cities";

const Daily = () => {
    const [visibleCities, setVisibleCities] = useState<string[]>([]);
    const [statsDataLoading, setStatsDataLoading] = useState(true);
    const [generalStatsData, setGeneralStatsData] = useState<GeneralStats | null>(null);
    const [statsData, setStatsData] = useState<Stats>({});
    const totalActiveCases = useMemo(
        () => generalStatsData ? generalStatsData.totalInfected - generalStatsData.totalRecovered - generalStatsData.totalDeaths : 0,
        [generalStatsData]
    );
    const totalActiveCasesIncrease = useMemo(
        () => generalStatsData ? generalStatsData.totalInfectedIncrease - generalStatsData.totalRecoveredIncrease - generalStatsData.totalDeathsIncrease : 0,
        [generalStatsData]
    );

    const cityStats = useMemo(() => {
        const stats = {} as CityStats;

        for (const date in statsData) {
            const dailyData = statsData[date];
            dailyData.forEach(value => {
                if (!stats[value.city]) {
                    stats[value.city] = {
                        count: 0
                    };
                }
                stats[value.city].count += value.count;
                stats[value.city].increase = isYesterday(new Date(date)) ? value.count : 0;
            });
        }
        return stats;
    }, [statsData]);
    const [menuOpened, openMenu, closeMenu] = useBooleanState();

    useEffect(() => {
        const loadGeneralStatsData = async () => {
            try {
                const data = await getGeneralStats();
                setGeneralStatsData(data);
            } catch (e) {
                // eslint-disable-next-line no-console
                console.error(e);
            }
        };
        loadGeneralStatsData();
    }, []);

    useEffect(() => {
        const loadStatsData = async () => {
            try {
                const statsData = await getDailyStats();
                setStatsData(statsData);
            } catch (e) {
                // eslint-disable-next-line no-console
                console.error(e);
            } finally {
                setStatsDataLoading(false);
            }
        };
        loadStatsData();
    }, []);

    useEffect(() => {
        const loadVisibleCities = async () => {
            try {
                const visibleCities = await storage.getItem(VISIBLE_CITIES_KEY);
                setVisibleCities(visibleCities as string[] || []);
            } catch (e) {
                // eslint-disable-next-line no-console
                console.error(e);
            }
        };
        loadVisibleCities();
    }, []);

    useEffect(() => {
        storage.setItem(VISIBLE_CITIES_KEY, visibleCities);
    }, [visibleCities]);

    const getCityData = (city: string) => {
        const data = [] as LineChartData[];
        let currentCount = 0;

        Object.keys(statsData).forEach(date => {
            const dateData = statsData[date];
            const cityData = dateData.find(({city: dateCity}) => dateCity == city);

            if (cityData) {
                data.push({
                    date: startOfDay(new Date(date)),
                    value: currentCount += cityData.count
                });
            } else if (isYesterday(new Date(date))) {
                data.push({
                    date: startOfYesterday(),
                    value: currentCount
                });
            }
        });

        return data;
    };

    const toCityChart = city => {
        const cityData = getCityData(city);

        return (
            <div key={city} className="cityChart">
                <Card size="small">
                    <div className="header">
                        <div className="cityName">{city}</div>
                        <div className="count">
                            {cityData[cityData.length - 1]?.value || <Spin/>}
                        </div>
                    </div>
                    <LineChart data={cityData}/>
                </Card>
            </div>
        );
    };

    return (
        <div className="daily">
            <header>
                <div className="title">COVID-19</div>
                <div className="subtitle">Slovakia</div>
                <div
                    className={`menuButton ${menuOpened ? "opened" : ""}`}
                    onClick={() => menuOpened ? closeMenu() : openMenu()}
                >
                    <div/>
                    <div/>
                    <div/>
                </div>
            </header>
            <section>
                <aside className={`sidebar ${menuOpened ? "opened" : ""}`}>
                    <CityTable
                        loading={statsDataLoading}
                        stats={cityStats}
                        selected={visibleCities}
                        selectionChanged={setVisibleCities}
                    />
                </aside>
                <div className="center">
                    <div className="generalStatistics">
                        <Card>
                            <Statistic
                                title="Total infected"
                                value={generalStatsData?.totalInfected}
                                valueRender={!generalStatsData ? () => <Spin/> : undefined}
                                suffix={!generalStatsData ? undefined : `(+${generalStatsData.totalInfectedIncrease})`}
                            />
                        </Card>
                        <Card>
                            <Statistic
                                title="Total recovered"
                                value={generalStatsData?.totalRecovered}
                                valueRender={!generalStatsData ? () => <Spin/> : undefined}
                                suffix={!generalStatsData ? undefined : `(+${generalStatsData.totalRecoveredIncrease})`}
                                valueStyle={{color: "#2c9c40"}}
                            />
                        </Card>
                        <Card>
                            <Statistic
                                title="Total deaths"
                                value={generalStatsData?.totalDeaths}
                                valueRender={!generalStatsData ? () => <Spin/> : undefined}
                                suffix={!generalStatsData ? undefined : `(+${generalStatsData.totalDeathsIncrease})`}
                                valueStyle={{color: "#bd2500"}}
                            />
                        </Card>
                        <Card>
                            <Statistic
                                title="Total active cases"
                                value={totalActiveCases}
                                valueRender={!generalStatsData ? () => <Spin/> : undefined}
                                prefix={!generalStatsData ? undefined : (totalActiveCasesIncrease > 0 ? <ArrowUpOutlined/> : <ArrowDownOutlined/>)}
                                suffix={!generalStatsData ? undefined : `(${totalActiveCasesIncrease > 0 ? "+" : ""}${totalActiveCasesIncrease})`}
                                valueStyle={{color: totalActiveCasesIncrease > 0 ? "#bd2500" : "#2c9c40"}}
                            />
                        </Card>
                    </div>
                    <div className="charts">
                        {visibleCities.map(toCityChart)}
                    </div>
                </div>
            </section>
        </div>
    );
};

export default Daily;
