export interface CityStats {
    [city: string]: {
        count: number;
        increase?: number;
    };
}
