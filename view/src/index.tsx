/* eslint-disable no-console */
import "./styles/global.scss";
import "antd/dist/antd.css";

import "core-js/stable";
import "regenerator-runtime/runtime";

import React from "react";
import {render} from "react-dom";
import {HashRouter} from "react-router-dom";

import {App} from "app/components";
import {Loading} from "common/components";

const root = document.getElementById("root");

if (root) {
    render(
        <React.Suspense
            fallback={
                <div className="expand">
                    <Loading/>
                </div>
            }
        >
            <HashRouter>
                <App/>
            </HashRouter>
        </React.Suspense>,
        root
    );
}

if (navigator.serviceWorker && process.env.IGNORE_SERVICE_WORKER !== "true") {
    window.addEventListener("load", () => {
        navigator.serviceWorker.register("/service-worker.js")
            .then(registration => console.log("SW registered: ", registration))
            .catch(error => console.log("SW registration failed: ", error));
    });
}
