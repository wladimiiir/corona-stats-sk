import React from "react";

import "./loading.scss";
import {Spin} from "antd";

type Props = {
    message?: string | null;
}

const Loading = ({message}: Props) => (
    <div className="loading">
        <div className="progressMessage">
            <Spin/>
            {message && <div className="message">{message}</div>}
        </div>
    </div>
);

export default Loading;
