import React from "react";
import {Route, Switch} from "react-router-dom";
import {Daily} from "stats/components";

const App = () => {
    return (
        <>
            <Switch>
                <Route path="/" component={Daily}/>
            </Switch>
        </>
    );
};

export default App;
