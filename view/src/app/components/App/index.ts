import {lazy} from "react";
import {hot} from "react-hot-loader";

export default hot(module)(lazy(() => import("./App" /* webpackChunkName: "app" */)));
