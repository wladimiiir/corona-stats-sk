import React, {useEffect, useRef} from "react";
import {parse, format} from "date-fns";
import Chart from "chart.js";
import "chartjs-adapter-date-fns";
import "chartjs-plugin-annotation";

export interface LineChartData {
    date: Date;
    value: number;
}

type Props = {
    data: LineChartData[];
    rightData?: LineChartData[];
    legends?: string[];
    units?: string[];
    referenceValue?: number | null;
};

const LineChart = ({data, rightData, legends, units, referenceValue}: Props) => {
    const canvasRef = useRef<HTMLCanvasElement>(null);
    const chartRef = useRef<Chart>(null);

    useEffect(() => {
        const datasets = [{
            data: [],
            fill: false,
            pointRadius: 1,
            backgroundColor: "#1b75bb",
            borderColor: "#1b75bb",
            borderWidth: 1,
            yAxisID: "yAxisLeft"
        }];
        const yAxes = [{
            id: "yAxisLeft",
            position: "left",
            gridLines: {
                drawOnChartArea: false
            }
        }];

        if (rightData) {
            datasets.push({
                data: [],
                fill: false,
                pointRadius: 1,
                backgroundColor: "#bb382b",
                borderColor: "#bb382b",
                borderWidth: 1,
                yAxisID: "yAxisRight"
            });
            yAxes.push({
                id: "yAxisRight",
                position: "right",
                gridLines: {
                    drawOnChartArea: false
                }
            });
        }

        chartRef.current = new Chart(canvasRef.current!, {
            type: "line",
            options: {
                aspectRatio: 3,
                legend: {
                    labels: {
                        filter: label => !!label.text
                    }
                },
                scales: {
                    xAxes: [{
                        type: "time",
                        time: {
                            unit: "day"
                        },
                        gridLines: {
                            drawOnChartArea: false
                        }
                    }],
                    yAxes
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                tooltips: {
                    intersect: false,
                    backgroundColor: "white",
                    borderColor: "#bdbabc",
                    borderWidth: 1,
                    xPadding: 8,
                    yPadding: 8,
                    titleFontColor: "#1b75bb",
                    titleFontFamily: "Roboto",
                    bodyFontColor: "#1b75bb",
                    bodyFontFamily: "Roboto",
                    footerFontColor: "#1b75bb",
                    displayColors: false,
                    callbacks: {
                        title: ([tooltipItem]) => format(parse(tooltipItem.label, "MMM d, y, h:mm:ss aaaa", new Date()), "MMM d")
                    }
                },
                annotation: {
                    drawTime: "afterDatasetsDraw",
                    annotations: [{
                        drawTime: "afterDraw",
                        type: "line",
                        mode: "horizontal",
                        scaleID: "yAxisLeft",
                        value: typeof referenceValue === "number" ? referenceValue : null,
                        borderColor: "#d6555f",
                        borderDash: [2, 2],
                        borderWidth: 1
                    }]
                }
            },
            data: {
                labels: [],
                datasets
            }
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [referenceValue]);

    useEffect(() => {
        const chart = chartRef.current!;

        chart.options.scales.yAxes.forEach((yAxis, index) => {
            yAxis.scaleLabel.display = units && units[index];
            yAxis.scaleLabel.labelString = units && units[index];
        });

        chart.update();
    }, [units]);

    useEffect(() => {
        const chart = chartRef.current!;

        chart.options.legend.display = !!legends;
        if (legends) {
            legends.forEach((label, index) => chart.data.datasets[index].label = label);
        }
        chart.update();
    }, [legends]);

    useEffect(() => {
        const chart = chartRef.current!;

        chart.data.labels = data.concat(rightData || []).map(dataItem => dataItem.date).sort();
        chart.data.datasets[0].data = data.map(dataItem => ({
            x: dataItem.date,
            y: dataItem.value
        }));
        if (rightData && chart.data.datasets[1]) {
            chart.data.datasets[1].data = rightData.map(dataItem => ({
                x: dataItem.date,
                y: dataItem.value
            }));
            chart.options.scales.yAxes[1].display = rightData.length > 0;
        }
        chart.update();
    }, [data, rightData]);

    return (
        <canvas className="lineChart" ref={canvasRef}>
        </canvas>
    );
};
export default LineChart;
