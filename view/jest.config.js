// eslint-disable-next-line @typescript-eslint/no-var-requires
const {defaults} = require("jest-config");

module.exports = {
    "roots": [
        "<rootDir>/test/unit"
    ],
    "modulePaths": [
        "<rootDir>/src"
    ],
    "moduleNameMapper": {
        "^.+\\.(css|less|scss)$": "identity-obj-proxy"
    },
    "transform": {
        "^.+\\.tsx?$": "ts-jest"
    },
    "testRegex": "(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$",
    "moduleFileExtensions": [...defaults.moduleFileExtensions, "ts", "tsx"],
    "setupFilesAfterEnv": [
        "@testing-library/react/cleanup-after-each",
        "@testing-library/jest-dom/extend-expect"
    ]
};
